<?php
Route::group(['namespace' => 'Bee\System\Http\Controllers\Auth', 'prefix' => config('cms.system.general.admin_dir'), 'middleware' => 'web'], function () {
    Route::get('login', 'AuthController@getLogin')->name('system.getLogin');
    Route::post('login', 'AuthController@postLogin')->name('system.postLogin');
});
Route::group(['namespace' => 'Bee\System\Http\Controllers', 'prefix' => config('cms.system.general.admin_dir'), 'middleware' => 'checkRoleAdmin'], function () {
    Route::get('', 'DashboardController@getIndex')->name('system.index');
});

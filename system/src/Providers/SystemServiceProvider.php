<?php

namespace Bee\System\Providers;

use Illuminate\Support\ServiceProvider;
use Bee\System\Traits\LoadAndPublishDataTrait as SystemTrait;
use Bee\System\Supports\Helper;
class SystemServiceProvider extends ServiceProvider
{
    use SystemTrait;
    public function boot()
    {
        $this->setNamespace('bee/system');
        $this->loadAndPublishConfigurations(['general']);
        $this->publishViews();
        $this->loadViews();
        $this->publishes([
            __DIR__.'/../../public' => public_path('vendor/system'),
        ], 'cms_public');
        $this->loadAndPublishTranslations();
        $this->loadRoutes(['web']);
        $this->loadMigrations();
    }
    public function register()
    {
        /*Autoload helpers from */
        Helper::autoload(__DIR__ . '/../../helpers');
    }
}

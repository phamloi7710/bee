<?php

namespace Bee\System\\Http;

use Illuminate\Foundation\Http\Kernel as HttpKernel;

class Kernel extends HttpKernel
{
    protected $routeMiddleware = [
        'checkRoleAdmin' => \Bee\System\Http\Middleware\CheckRoleAdmin::class,
    ];
}

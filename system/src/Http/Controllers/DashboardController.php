<?php
    namespace Bee\System\Htp\Controllers
    use Bee\System\Http\Controllers\BaseController;
    class DashboardController extends BaseController
    {
        public function getIndex()
        {
            return view('bee.system::index');
        }
    }
?>

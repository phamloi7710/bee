<?php
    namespace Bee\System\Http\Controllers\Auth;
    use Bee\System\Http\Controllers\BaseController;
    use Illuminate\Http\Request;
    use App\User;
    use Illuminate\Support\Facades\Auth as Authentication;
    use Illuminate\Support\Facades\Input;
    use Illuminate\Support\Facades\Redirect;
    use Session;
    use Illuminate\Support\Facades\Password;
    use Illuminate\Foundation\Auth\SendsPasswordResetEmails;
    use Illuminate\Foundation\Auth\ResetsPasswords;
    use Illuminate\Support\Str;
    use Illuminate\Support\Facades\Mail;
    use App;
    use DB;
    class AuthController extends BaseController
    {
        public function getLogin()
        {
            if (Authentication::check()) {
                return redirect()->route('getIndexAdmin');
            }else{
                return view('cms.system::auth.login');
            }
        }
        public function postLogin(Request $request)
        {
            $data = [
                'username' => $request->txtUsername,
                'password' => $request->txtPassword,
            ];
            if(Authentication::attempt($data)){
                $notifySuccess = array(
                    'message' => __('cms/system::message.login.success'),
                    'alert-type' => 'success',
                );
                return redirect()->route('getIndexAdmin')->with($notifySuccess);
            }
            else{
                $notifySuccess = array(
                    'message' => __('cms/system::message.login.failed'),
                    'alert-type' => 'error',
                );
                return redirect()->back()->with($notifySuccess);
            }
        }
    }
?>

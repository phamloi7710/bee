<?php

namespace Bee\System\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;
class CheckRoleAdmin
{
    public function handle($request, Closure $next)
    {
        if(Auth::check())
        {
            if(Auth::user()){
                return $next($request);
            }else{
                return redirect()->back();
            }
        }else{
            return redirect()->route('getLoginAdmin')->with('warning', 'Bạn chưa đăng nhập vào hệ thống!');
        }
    }
}

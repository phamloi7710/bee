<?php
    return [
        "title" => "Đăng Nhập Hệ Thống",
        'username' => 'Tên Đăng Nhập',
        'password' => 'Mật Khẩu',
        'box-msg' => 'Đăng nhập để bắt đầu phiên của bạn',
        'sign-in' => 'Đăng Nhập',
        'remember-me' => 'Ghi Nhớ Đăng Nhập'
    ]
?>

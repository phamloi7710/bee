@section('title')
@lang('cms/system::login.title')
@stop
@extends('cms.system::auth.master')
@section('content')
    <div class="login-box-body">
        <p class="login-box-msg">@lang('cms/system::login.box-msg')</p>
        <form action="{{route('system.postLogin')}}" method="POST">
            @csrf
            <div class="form-group has-feedback">
                <input name="txtUsername" type="text" class="form-control" placeholder="@lang('cms/system::login.username')">
                <span class="glyphicon glyphicon-user form-control-feedback"></span>
            </div>
            <div class="form-group has-feedback">
                <input name="txtPassword" type="password" class="form-control" placeholder="@lang('cms/system::login.password')">
                <span class="glyphicon glyphicon-lock form-control-feedback"></span>
            </div>
            <div class="row">
                <div class="col-xs-8">
                    <div class="checkbox icheck">
                        <label>
                            <input type="checkbox"> @lang('cms/system::login.remember-me')
                        </label>
                    </div>
                </div>
                <!-- /.col -->
                <div class="col-xs-4">
                    <button type="submit" class="btn btn-primary btn-block btn-flat">@lang('cms/system::login.sign-in')</button>
                </div>
                <!-- /.col -->
            </div>
        </form>
        <a href="#">I forgot my password</a><br>
        <a href="register.html" class="text-center">Register a new membership</a>

    </div>
@stop
